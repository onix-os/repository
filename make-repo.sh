#!/bin/bash
LOG="$WORKDIR/logs/make-repo-$(date +%Y%M%d%H%m).log"
WORKDIR=$(pwd)

source $WORKDIR/util/repo.sh

mkrepo $WORKDIR "repo"
