#!/bin/bash
WORKDIR=$(pwd)
LOG="$WORKDIR/logs/kernel-$(date +%Y%M%d%H%m).log"
KERNEL="linux-onix"
source $WORKDIR/util/pkgmake.sh

pkgmake $WORKDIR "packages" $KERNEL "repo" $LOG