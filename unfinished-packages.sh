#!/bin/bash
WORKDIR=$(pwd)

source $WORKDIR/util/unfinished.sh

status=false
while read line; do
	status=$(unfinished_repo $WORKDIR "repo" $line)

	if [[ "${status}" == "false" ]]; then
		echo "Package $line not found."
	fi
	
done < $WORKDIR/packages.list

status=false
while read line; do
	status=$(unfinished_repo $WORKDIR "repo" $line)

	if [[ "${status}" == "false" ]]; then
		echo "Package $line not found."
	fi
done < $WORKDIR/aur.list
