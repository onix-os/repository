#!/bin/bash
export LC_ALL=C
WORKDIR=$(pwd)
source $WORKDIR/util/sync.sh

SERVER="192.168.1.35"
USER="onix-project"
PATH="/home/onix-project/domains/repo.onix-project.com/public_html/"
PORT="22"
FOLDER="repo"

sync $WORKDIR $FOLDER $PORT $USER $SERVER $PATH
