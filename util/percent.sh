#!/bin/bash

round()
{
    echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
};

percent()
{
    result=$(echo "scale=4; 100/$1" | bc -l)
    result=$(echo "scale=4; $2*$result" | bc -l)
    result=$(round $result 0)
    echo "$result%"
}