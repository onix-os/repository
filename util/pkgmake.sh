#!/bin/bash

# ussage
# pkgmake workdir folder pkgname repo log
pkgmake() {
    WORKDIR=$1
    FOLDER=$2
    PKGNAME=$3
    REPO=$4
    LOG=$5
    OPTION=$6
    PKGDIR=$WORKDIR/$FOLDER/$PKGNAME
    if [ -d $PKGDIR ]
    then
        cd $PKGDIR
        if [ -f PKGBUILD ]
        then
            source PKGBUILD
            rm -rfv pkg/* &>> $LOG
            rm -rfv *.tar.* &>> $LOG
            _installMany "${makedepends[@]}" &>> $LOG
            _installMany "${depends[@]}" &>> $LOG
            if [ ! -z $OPTION ]
            then
                makepkg -s -f --noconfirm $OPTION &>> $LOG
            else
                makepkg -s -f --noconfirm &>> $LOG
            fi
            cd $WORKDIR/
            if [[ -n $(find $PKGDIR/ -name "*.pkg.*") ]]
            then
                cp -av $PKGDIR/*.pkg.* $WORKDIR/$REPO/ &>> $LOG
            fi
        fi
    fi
}


_isInstalled() {
    package="$1";
    check="$(sudo pacman -Qs --color always "${package}" | grep "local" | grep "${package} ")";
    if [ -n "${check}" ] ; then
        echo 0; #'0' means 'true' in Bash
        return; #true
    fi;
    echo 1; #'1' means 'false' in Bash
    return; #false
}

# `_install <pkg>`
_install() {
    package="$1";

    # If the package IS installed:
    if [[ $(_isInstalled "${package}") == 0 ]]; then
        echo "${package} is already installed.";
        return;
    fi;

    # If the package is NOT installed:
    if [[ $(_isInstalled "${package}") == 1 ]]; then
        sudo pacman -Syu --noconfirm --needed --asdeps --overwrite "*" "${package}";
    fi;
}

# `_installMany <pkg1> <pkg2> ...`
# Works the same as `_install` above,
#     but you can pass more than one package to this one.
_installMany() {
    # The packages that are not installed will be added to this array.
    toInstall=();

    for pkg; do
        # If the package IS installed, skip it.
        if [[ $(_isInstalled "${pkg}") == 0 ]]; then
            echo "${pkg} is already installed.";
            continue;
        fi;

        #Otherwise, add it to the list of packages to install.
        if [[ $(_checkRepoExist "${pkg}") == "${pkg}" ]]; then
            toInstall+=("${pkg}");
        else
            echo "Package ${pkg} is not available in the Arch repository.";
        fi;
    done;

    # If no packages were added to the "${toInstall[@]}" array,
    #     don't do anything and stop this function.
    if [[ "${toInstall[@]}" == "" ]] ; then
        echo "All packages are already installed.";
        return;
    fi;

    # Otherwise, install all the packages that have been added to the "${toInstall[@]}" array.
    printf "Packages not installed:\n%s\n" "${toInstall[@]}";
    sudo pacman -Syu --noconfirm --needed --asdeps --overwrite "*" "${toInstall[@]}";
}

_checkRepoExist() { 
    echo $(pacman -Ssq "$*" | grep "$*")
}