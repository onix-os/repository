#!/bin/bash
shopt -s nullglob

unfinished()
{
    WORKDIR=$1
    FOLDER=$2
    PKGNAME=$3
    if [ -d "$WORKDIR/$FOLDER/$PKGNAME/" ]; then
		pkgs=$(find $WORKDIR/$FOLDER/$PKGNAME/ -maxdepth 1 -type f -name "*.pkg.*" | wc -l)
	
		if [ $pkgs -eq 0 ]; then
			echo false
		else
			echo true
		fi
	fi
}

unfinished_repo()
{
    WORKDIR=$1
    FOLDER=$2
    PKGNAME=$3
    if [ -d "$WORKDIR/$FOLDER/" ]; then
		pkgs=$(find $WORKDIR/$FOLDER/ -maxdepth 1 -type f -name "$PKGNAME*" | wc -l)
	
		if [ $pkgs -eq 0 ]; then
			echo false
		else
			echo true
		fi
	fi
}