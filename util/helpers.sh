#!/bin/bash

listhugedir()
{
    HUGEDIR=$1
    du -hsm $HUGEDIR | sort -nr | head -10
}