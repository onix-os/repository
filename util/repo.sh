#!/bin/bash

mkrepo()
{
    WORKDIR=$1
    FOLDER=$2
    cd $WORKDIR/$FOLDER/
    repo-add onix.db.tar.gz ./*.pkg.*
    cd $WORKDIR
}

addtorepo()
{
    PKG=$1
    WORKDIR=$2
    FOLDER=$3
    cd $PKG
    mv -av *.pkg.* $WORKDIR/$FOLDER/
    cd $WORKDIR
    mkrepo $WORKDIR $FOLDER
}
