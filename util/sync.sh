#!/bin/bash
# WORKDIR=$(pwd)

sync() {
    WORKDIR=$1
    FOLDER=$2
    PORT=$3
    USER=$4
    SERVER=$5
    PATH=$6

    #/usr/bin/scp -P $PORT $WORKDIR/$FOLDER/* $USER@$SERVER:$PATH
    /usr/bin/rsync -avuP -e "/usr/bin/ssh -p $PORT" $WORKDIR/repo/* $USER@$SERVER:$PATH
}
