#!/bin/bash
WORKDIR=$(pwd)
LOG="$WORKDIR/logs/aur-$(date +%Y%M%d%H%m).log"
TOTAL=$(cat aur.list | wc -l)
i=0
source $WORKDIR/util/percent.sh
source $WORKDIR/util/pkgmake.sh

if [ ! -d $WORKDIR/aur-packages ];then
	mkdir -p $WORKDIR/aur-packages
fi

rm -rf $WORKDIR/aur-packages/*

while read line; do
	i=$((i+1))
	echo "[$i/$TOTAL][$line] Building... [$(percent $TOTAL $i)]"
	PACKAGE=$line

	if [ ! -d $WORKDIR/aur-packages/$PACKAGE ];then
		git clone https://aur.archlinux.org/$PACKAGE.git $WORKDIR/aur-packages/$PACKAGE &>> $LOG 
	else
		cd $WORKDIR/aur-packages/$PACKAGE
		git reset --hard
		git pull -ff
	fi

	pkgmake $WORKDIR "aur-packages" $PACKAGE "repo" $LOG
done < $WORKDIR/aur.list
