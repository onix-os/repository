#!/bin/bash
WORKDIR=$(pwd)
source $WORKDIR/util/error.sh

filename=$1
if [ ! -z $filename ]
then
	cat $(latest "$WORKDIR/logs" $filename) | grep -e "is not available in the Arch repository"
else
	cat $(latest "$WORKDIR/logs" $filename) | grep -e "is not available in the Arch repository"
fi