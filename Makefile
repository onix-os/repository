deps:
	/bin/bash ./install-deps.sh

clean:
	/bin/bash ./clean.sh

kernel:
	/bin/bash ./kernel-build.sh

package:
	/bin/bash ./packages-build.sh

aur:
	/bin/bash ./aur-build.sh


sf_sync:
	/bin/bash ./sf-sync.sh

server_sync:
	/bin/bash ./server-sync.sh

make_repo:
	/bin/bash ./make-repo.sh

build: package aur make_repo

all: build server_sync

upload: server_sync sf_sync

docker:
	/bin/bash ./docker-run.sh

git_update: 
	git add -A 
	git commit -m "repo and scripts update"
	git push

git_pull:
	git reset --hard
	git pull -ff

