#!/bin/bash
WORKDIR=$(pwd)
LOG="$WORKDIR/logs/packages-$(date +%Y%M%d%H%m).log"
TOTAL=$(cat packages.list | wc -l)
i=0
source $WORKDIR/util/percent.sh
source $WORKDIR/util/pkgmake.sh

while read line; do
        i=$((i+1))
	echo "[$i/$TOTAL][$line] Building... [$(percent $TOTAL $i)]"
        pkgmake $WORKDIR "packages" $line "repo" $LOG "-c"
done < $WORKDIR/packages.list
