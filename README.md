Onix OS Repository
-----------------------------------

Onix OS Repository Build Scripts for Arch Linux. And also contains Onix OS base packages or other packages etc.

Install depends;

```
$ make deps
```

Build;

```
$ make build
```

or 

```
$ make all
```

Upload;

```
$ make upload
```

All in One;

```
$ make deps all upload
```

Kernel;

```
$ make kernel
```

This makefile; make packages, aur packages and kernel and upload main/sourceforge server.


Repository for Arch Linux;

```
[onix]
SigLevel = Never
Server = http://repo.onix-project.com
Server = https://sourceforge.net/projects/onixos/files/Repo/
```