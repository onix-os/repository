#!/bin/bash
WORKDIR=$(pwd)

source $WORKDIR/util/percent.sh

cd $WORKDIR

if [ ! -w $WORKDIR/repo ];then
	echo "You don't have write permission to $WORKDIR/repo"
	echo "We can delete with sudo $WORKDIR/repo."
	sudo rm -rf $WORKDIR/repo
fi

if [ ! -d $WORKDIR/repo ]; then
	mkdir -p $WORKDIR/repo
fi

TOTAL=$(cat aur.list | wc -l)
i=0
while read line; do
	i=$((i+1))
	echo "[$i/$TOTAL][$line] Cleaning... [$(percent $TOTAL $i)]"
	if [ -d  $WORKDIR/aur-packages/$line ]; then
		cd $WORKDIR/aur-packages/
		rm -rf $line
		#rm -rf *.pkg.* 
		#rm -rf pkg/
		cd $WORKDIR
	fi
done < $WORKDIR/aur.list

#rm -rf $WORKDIR/aur-packages/*

TOTAL=$(cat packages.list | wc -l)
i=0
while read line; do
	i=$((i+1))
	echo "[$i/$TOTAL][$line] Cleaning... [$(percent $TOTAL $i)]"
	if [ -d  $WORKDIR/packages/$line ]; then
		cd $WORKDIR/packages/$line
		rm -rf *.pkg.* 
		rm -rf pkg/
		rm -rf src/
		cd $WORKDIR
	fi
done < $WORKDIR/packages.list

KERNEL="linux-onix"
if [ -d $WORKDIR/packages/$KERNEL ];
then
	echo "[1/1][$KERNEL] Cleaning... [$(percent $TOTAL $i)]"
    cd $WORKDIR/packages/$KERNEL
    rm -rf *.pkg.* 
	rm -rf pkg/
	rm -rf src/
	rm -rf *.tar.*
	cd $WORKDIR
fi

rm -rf $WORKDIR/logs/*.log
