#!/bin/env bash

docker-compose up --build -d
docker-compose exec onixrepo make all
#docker-compose exec onixrepo bash
docker-compose down
