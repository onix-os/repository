FROM olproject/onixos:latest

RUN pacman -Syu --noconfirm yay-git rsync go xmlto python-sphinx base-devel go gnome-shell git qt qtcreator gperf ninja nasm openssh openssl sudo python nodejs node-gyp archiso python --overwrite "*"
RUN echo "buildenv ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN groupadd buildenv
RUN useradd -ms /bin/bash buildenv -g buildenv
RUN chown -Rv buildenv:buildenv /home/buildenv
USER buildenv
COPY --chown=buildenv . /home/buildenv/repository
WORKDIR /home/buildenv/repository
ENV PATH=$PATH:/usr/bin:/usr/sbin:/bin:/home/buildenv/go/bin
CMD ["make", "build"]
VOLUME ["/home/buildenv"]
