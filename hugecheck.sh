#!/bin/bash
WORKDIR=$(pwd)

source $WORKDIR/util/helpers.sh

listhugedir "$WORKDIR/aur-packages/*"

listhugedir "$WORKDIR/packages/*"

listhugedir "$WORKDIR/repo/*"