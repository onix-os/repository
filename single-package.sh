#!/bin/bash
WORKDIR=$(pwd)

source $WORKDIR/util/pkgmake.sh
source $WORKDIR/util/repo.sh

if [ ! -z $1 ];then
    PACKAGE=$1
    LOG="$WORKDIR/logs/$PACKAGE-$(date +%Y%M%d%H%m).log"
    pkgmake $WORKDIR "packages" $1 "repo" $LOG
    addtorepo "$WORKDIR/packages/$PACKAGE" $WORKDIR "repo"
else
	echo "Usage: $0 <package-name>"
fi
